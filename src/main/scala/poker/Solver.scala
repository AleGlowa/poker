package poker

object Solver {
  import scala.util.{Try, Success, Failure}
  import poker.Card.{ranks, units}

  def process(line: String): String = {
    val ErrorPrefix = "Error: "

    line.toLowerCase.split("\\s+").toList match {
      case "texas-holdem" :: board :: hands =>
        decode(TexasHoldem.sortHandsByStrength(encodeBoard(board), hands map encodeHand))
      case "omaha-holdem" :: board :: hands =>
        decode(OmahaHoldem.sortHandsByStrength(encodeBoard(board), hands map encodeHand))
      case "five-card-draw" :: hands =>
        decode(FiveCardDraw.sortHandsByStrength(Set(), hands map encodeHand))
      case _ :: _ => ErrorPrefix + "Unrecognized game type"
      case _ => ErrorPrefix + "Invalid input"
    }
  }

  /** Returns object representation of `rawCard`
   *  e.g. "Ac" -> Card('A','c')
   */
  private def encodeCard(rawCard: String): Try[Card] = {
    val rank = if (rawCard(0).isLetter) rawCard(0).toUpper else rawCard(0)
    val unit = rawCard(1)
    if (ranks(rank) && units(unit))
      Success(Card(rank, unit))
    else
      Failure(new IllegalArgumentException)
  }

  /** Returns encoded hand as a list of cards.
   *  List because it has to preserve order of cards
   *  e.g. hand holding 2 cards
   *    "Ac9s" -> List(Card('A','c'), Card('9','s'))
   */
  private def encodeHand(rawHand: String): Hand =
    ((rawHand grouped 2) map { encodeCard(_) match {
      case Success(card) => card
      case Failure(e) =>
        println("Invalid rank or unit of a card!")
        throw e
    }}).toList

  /** Returns encoded board as a set of cards.
   *  Set because cards on the board don't need to be ordered.
   *  e.g. 3 cards laying on the board
   *    "Ac9s8s" -> Set(Card('9','s'), Card('A','c'), Card('8','s'))
   */
  private def encodeBoard(rawBoard: String): Board =
    ((rawBoard grouped 2) map { encodeCard(_) match {
      case Success(card) => card
      case Failure(e) =>
        println("Invalid rank or unit of a card!")
        throw e
    }}).toSet

  /** Returns a string representation of result putting '=' sign between hands with the same strengths.
   *
   *  This operation can be divided to 2 parts.
   *  1. Creates another list from foldLeft method in format like this:
   *     List(List(Ad4s,Ac4d), List(5d6d), List(As9s), List(KhKd))
   *     where each list with more than 1 hand, its hands are consider equal to each other
   *  2. Creates a string by sorting alphabetically the above list's elements with size > 1,
   *     and then putting '=' between hands which are in the same list.
   * */
  private def decode(xs: List[(Hand, HandValueWithCards)])(implicit ord: Ordering[HandValueWithCards]): String = {
    // First part
    val mapping = xs.toMap
    val tempList = xs.tail.foldLeft(List(List(xs.head._1))) { case (x, y) =>
      if (ord.equiv(mapping(x.last.last), y._2)) x.init :+ (x.last :+ y._1)
      else x :+ List(y._1)
    }

    // Second part
    (tempList map (_ map (_.mkString)) map  (x => if (x.size > 1) x.sorted.mkString("=") else x.mkString)).mkString(" ")
  }
}
