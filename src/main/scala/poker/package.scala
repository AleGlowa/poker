/** Provides the companion object which defines certain implicit orderings for specific types
 *  to write the solution in `GameTypes` file cleaner, so more readable. Also provides mapping
 *  from rank to integer for comparing cards.
 */
package object poker {

  case class Card(rank: Char, unit: Char) {
    override def toString: String =  s"$rank$unit"

    // For easier implementation of Ordering[HandValueWithCards]
    def > (that: Card)(implicit ord: Ordering[Card]): Boolean = ord.gt(this, that)
    def < (that: Card)(implicit ord: Ordering[Card]): Boolean = ord.lt(this, that)
  }

  object Card {
    /** Defines sort operation for cards based on their ranks */
    implicit val rankOrder: Ordering[Card] = (c1: Card, c2: Card) =>
      rankToInt(c1.rank) - rankToInt(c2.rank)

    /** Defines sort operation for pairs of hand value's name and corresponding set of cards.
     *  It works like this:
     *
     *  {{{
     *  val s1 = Set(Card('A','s'),Card('J','s'),Card('Q','h'),Card('K','s'),Card('T','s'))
     *  val str1 = (HandValues.Straight, s1)
     *  val s2 = Set(Card('K','d'),Card('T','d'),Card('J','d'),Card('Q','d'),Card('9','h'))
     *  val str2 = (HandValues.Straight, s2)
     *  handValueOrder.gt(str1, str2) shouldBe true
     *  }}}
     */
    implicit val handValueOrder: Ordering[HandValueWithCards] = (hvcs1: HandValueWithCards, hvcs2: HandValueWithCards) => {
      val (hv1, cs1) = hvcs1
      val (hv2, cs2) = hvcs2
      // Defines sorted cards from highest rank for maxHighCard method
      val sortedCs1 = cs1.toList sortWith (_ > _)
      val sortedCs2 = cs2.toList sortWith (_ > _)

      if (hv1 > hv2) 1
      else if (hv1 < hv2) -1
      // If hand value's names are equal 'else part' compares combinations
      else hv1 match {
        case HandValues.HighCard | HandValues.Flush =>
          maxHighCard(sortedCs1, sortedCs2)

        case HandValues.Pair =>
          val pairVal1 = getNOfKind(cs1, 2)(0).head
          val pairVal2 = getNOfKind(cs2, 2)(0).head

          if (pairVal1 > pairVal2) 1
          else if (pairVal1 < pairVal2) -1
          else maxHighCard(sortedCs1, sortedCs2)

        case HandValues.TwoPairs =>
          val (pairs1, pairs2) = (getNOfKind(cs1, 2).flatten, getNOfKind(cs2, 2).flatten)
          val (firstPairVal1, firstPairVal2) = (pairs1.max, pairs2.max)
          val (secondPairVal1, secondPairVal2) = (pairs1.min, pairs2.min)

          if (firstPairVal1 > firstPairVal2) 1
          else if (firstPairVal1 < firstPairVal2) -1
          else {
            if (secondPairVal1 > secondPairVal2) 1
            else if (secondPairVal1 < secondPairVal2) -1
            else maxHighCard(sortedCs1, sortedCs2)
          }

        case HandValues.ThreeOfKind =>
          val threeVal1 = getNOfKind(cs1, 3)(0).head
          val threeVal2 = getNOfKind(cs2, 3)(0).head

          if (threeVal1 > threeVal2) 1
          else if (threeVal1 < threeVal2) -1
          else maxHighCard(sortedCs1, sortedCs2)

        case HandValues.Straight | HandValues.StraightFlush =>
          val (lowestRank1, lowestRank2) = (cs1.min, cs2.min)

          if (lowestRank1 > lowestRank2) 1
          else if (lowestRank1 < lowestRank2) -1
          else {
            // Compare the exception straight (A,2,3,4,5)
            if (cs1.max.rank == 'A' && lowestRank1.rank == '2' && cs2.max.rank != 'A') -1
            else 0
          }

        case HandValues.FullHouse =>
          val (threeVal1, pairVal1) = (getNOfKind(cs1, 3)(0).head, getNOfKind(cs1, 2)(0).head)
          val (threeVal2, pairVal2) = (getNOfKind(cs2, 3)(0).head, getNOfKind(cs2, 2)(0).head)

          if (threeVal1 > threeVal2) 1
          else if (threeVal1 < threeVal2) -1
          else {
            if (pairVal1 > pairVal2) 1
            else if (pairVal1 < pairVal2) -1
            else 0
          }

        case HandValues.FourOfKind =>
          val fourVal1 = getNOfKind(cs1, 4)(0).head
          val fourVal2 = getNOfKind(cs2, 4)(0).head

          if (fourVal1 > fourVal2) 1
          else if (fourVal1 < fourVal2) -1
          else maxHighCard(sortedCs1, sortedCs2)
      }
    }

    /** Defines strength for each rank */
    lazy val rankToInt: Map[Char, Int] = Map('2' -> 2, '3' -> 3, '4' -> 4, '5' -> 5, '6' -> 6,
                                             '7' -> 7, '8' -> 8, '9' -> 9, 'T' -> 10, 'J' -> 11,
                                             'Q' -> 12, 'K' -> 13, 'A' -> 14)

    lazy val ranks: Set[Char] = rankToInt.keySet
    lazy val units: Set[Char] = Set('h', 'd', 'c', 's')
    /** Returns which player has larger high card
     *  Input has to be 2 sorted lists of cards with equal lengths
     */
    @scala.annotation.tailrec
    private def maxHighCard(cs1: List[Card], cs2: List[Card]): Int = (cs1, cs2) match {
      case (d1 :: ds1, d2 :: ds2) => if (d1 > d2) 1 else if (d1 < d2) -1 else maxHighCard(ds1, ds2)
      case _ => 0
    }

    /** Returns a vector of pairs for `n` = 2 pulled out from `cards`
     *          a vector of three of a kind for `n` = 3
     *          a vector of four of a kind for `n` = 4
     */
    def getNOfKind(cards: Set[Card], n: Int): Vector[Set[Card]] =  {
      val unsorted = cards
        .groupBy(_.rank)
        .filter { case (_, v) => v.size == n }
      unsorted.toVector map (_._2)
    }
  }

  /** Defines the enumeration of strengths of hand value's names */
  object HandValues extends Enumeration {
    type HandValue = Value

    val StraightFlush: HandValue = Value(8)
    val FourOfKind: HandValue = Value(7)
    val FullHouse: HandValue = Value(6)
    val Flush: HandValue = Value(5)
    val Straight: HandValue = Value(4)
    val ThreeOfKind: HandValue = Value(3)
    val TwoPairs: HandValue = Value(2)
    val Pair: HandValue = Value(1)
    val HighCard: HandValue = Value(0)
  }

  type Hand = List[Card]
  type Board = Set[Card]
  type HandValueWithCards = (HandValues.HandValue, Set[Card])
}
