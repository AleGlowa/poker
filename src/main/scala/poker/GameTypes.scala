package poker

/** Gives all needed functionalities for game types
 *
 *  `numFromBoard` and `numFromHand` must be passed in order to
 *  initiate a new game type. Former is the number of cards which have to
 *  be taken from the board. Latter is the number of cards which have to
 *  be taken from the hand. These combined cards finally form a hand value
 *
 *  1. Texas-holdem:
 *    numFromBoard = 5
 *    numFromHand = 2
 *  2. Omaha-holdem:
 *    numFromBoard = 3
 *    numFromHand = 2
 *  3. Five-card-draw:
 *    numFromBoard = 0
 *    numFromHand = 5
 */
abstract class GameType(numFromBoard: Int, numFromHand: Int) {
  // Hand value's name
  import HandValues.HandValue

  /** Returns a sorted hands according to their strength
   *
   *  @return Returns a list of pairs where first value `Hand` is a hand at the beginning of the game,
   *          where the second value is another pair `HandValueWithCards` with best
   *          `Hand` value's name and the corresponding set of cards.
   */
  def sortHandsByStrength(board: Board, hands: List[Hand]): List[(Hand, HandValueWithCards)] = {
    val unsortedList = for {
      hand <- hands
    } yield hand -> maxHandValue(board, hand.toSet) // Calculate for each hand its best hand value name
                                                    // and corresponding set of cards
    unsortedList sortBy (_._2)
  }

  /** Returns best hand value's name and corresponding set of cards */
  private def maxHandValue(board: Board, hand: Set[Card]): HandValueWithCards = {
    def maxHandValueSub(cards: Set[Card]): HandValue = {
      // Search for best possible hand value's name by checking conditions
      (for {
        v <- HandValues.values
      } yield v match {
        case HandValues.StraightFlush => straightFlush(cards)
        case HandValues.FourOfKind => fourOfKind(cards)
        case HandValues.FullHouse => fullHouse(cards)
        case HandValues.Flush => flush(cards)
        case HandValues.Straight => straight(cards)
        case HandValues.ThreeOfKind => threeOfKind(cards)
        case HandValues.TwoPairs => twoPairs(cards)
        case HandValues.Pair => pair(cards)
        case HandValues.HighCard => Some(HandValues.HighCard)
      }).max.get
    }

    // Checks all combinations (21)
    (for {
      // Takes an arbitrary number of cards from the board
      subsetFromBoard <- board.subsets(numFromBoard)
      // Takes an arbitrary number of cards from the hand
      subsetFromHand <- hand.subsets(numFromHand)
      // Combines them and checks every possible subset of size 5 from their union
      subset <- (subsetFromBoard union subsetFromHand).subsets(5)
    } yield (maxHandValueSub(subset), subset)).max  // Takes the strongest hand value from a given hand
  }

  // Hands always have size of 5 when checking hand values
  /** Returns hand value's name if the given `cards` meets the condition of having exactly one pair,
   *  otherwise returns None
   */
  protected def pair(cards: Set[Card]): Option[HandValue] = {
    val pairs = Card.getNOfKind(cards, 2)
    if (pairs.size == 1) Some(HandValues.Pair)
    else None
  }

  /** Returns hand value's name if the given `cards` meets the condition of having two pairs,
   *  otherwise returns None
   */
  protected def twoPairs(cards: Set[Card]): Option[HandValue] = {
    val pairs = Card.getNOfKind(cards, 2)
    if (pairs.size == 2) Some(HandValues.TwoPairs)
    else None
  }

  /** Returns hand value's name if the given `cards` meets the condition of having three of a kind,
   *  otherwise returns None
   */
  protected def threeOfKind(cards: Set[Card]): Option[HandValue] = {
    val three = Card.getNOfKind(cards, 3)
    if (three.nonEmpty) Some(HandValues.ThreeOfKind)
    else None
  }

  /** Returns hand value's name if the given `cards` meets the condition of having straight,
   *  otherwise returns None
   */
  protected def straight(cards: Set[Card]): Option[HandValue] = {
    val sortedCards = cards.toVector.sorted
    val isStraight = sortedCards match {
      // Checks an exception of straight where the ace is at the beginning (A,2,3,4,5)
      case Vector(Card('2',_), Card('3',_), Card('4',_), Card('5',_), Card('A',_)) => true
      case cs =>
        cs sliding 2 forall {
          case Vector(Card(r1, _), Card(r2, _)) => Card.rankToInt(r2) - Card.rankToInt(r1) == 1
        }
    }
    if (isStraight) Some(HandValues.Straight)
    else None
  }

  /** Returns hand value's name if the given `cards` meets the condition of having flush,
   *  otherwise returns None
   */
  protected def flush(cards: Set[Card]): Option[HandValue] = {
    if ((cards groupBy (_.unit)).values.size == 1) Some(HandValues.Flush)
    else None
  }

  /** Returns hand value's name if the given `cards` meets the condition of having full house,
   *  otherwise returns None
   */
  protected def fullHouse(cards: Set[Card]): Option[HandValue] = (threeOfKind(cards), pair(cards)) match {
    case (Some(_), Some(_)) => Some(HandValues.FullHouse)
    case _ => None
  }

  /** Returns hand value's name if the given `cards` meets the condition of having four of a kind,
   *  otherwise returns None
   */
  protected def fourOfKind(cards: Set[Card]): Option[HandValue] = {
    val four = Card.getNOfKind(cards, 4)
    if (four.nonEmpty) Some(HandValues.FourOfKind)
    else None
  }

  /** Returns hand value's name if the given `cards` meets the condition of having straight flush,
   *  otherwise returns None
   */
  protected def straightFlush(cards: Set[Card]): Option[HandValue] = (straight(cards), flush(cards)) match {
    case (Some(_), Some(_)) => Some(HandValues.StraightFlush)
    case _ => None
  }
}

object TexasHoldem extends GameType(5, 2)

object OmahaHoldem extends GameType(3, 2)

object FiveCardDraw extends GameType(0, 5)
